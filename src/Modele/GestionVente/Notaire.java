package Modele.GestionVente;


import Modele.Agence.Agence;

public class Notaire {
   //Attributs
   private String adresseNotaire;
   private float comissionAgence;
   private boolean commissionVersee = false;
   private float sommeNotaire;

   //Constructeur
   public Notaire(String adresseNotaire){
      this.adresseNotaire = adresseNotaire;
   }
   //Assesseurs
   //Getter
   public String getAdresseNotaire() { return adresseNotaire; }
   public float getComissionAgence() { return comissionAgence;}

   //Methodes
   public boolean comissionEstVersee(){ return this.commissionVersee; }

   public void verserCommissionAgence(Agence monAgence, float valeurComission){
      if(!this.commissionVersee) {
         this.comissionAgence = valeurComission;
         monAgence.augmenterSommeAgence(valeurComission);
         this.commissionVersee = true;
      }

   }

}
