package Modele.GestionVente;

import Modele.GestionClient.Client;

import java.time.LocalDate;

public class PromesseVente extends Notaire{
   //Attribus
   private float prixVerserAuVendeur;
   private LocalDate dateVente;
   private float commissionAgence;
   private float fraisVente;
   private boolean signaturePromesseVente = false;

   //Contructeur
   public PromesseVente(float prixVerserAuVendeur, String adresseNotaire, LocalDate dateVente, float fraisVente, float comissionAgence){
      super(adresseNotaire);
      this.prixVerserAuVendeur = prixVerserAuVendeur;
      this.dateVente = dateVente;
      this.fraisVente = fraisVente;
      this.commissionAgence = comissionAgence;
   }
   //Assesseur

   public float getPrixVerserAuVendeur() { return prixVerserAuVendeur;}
   public LocalDate getDateVente() { return dateVente;}
   public float getCommissionAgence() { return commissionAgence;}
   public float getFraisVente() { return fraisVente;}
   public boolean aSignerPromesseVente() { return signaturePromesseVente; }

   //Methodes
   public void SignerPromesseDeVente(Client monAcheteur, Client monVendeur){
      monAcheteur.signerPromesseVenteAcheteur();
      monVendeur.signerPromesseVenteVendeur();
      this.signaturePromesseVente = true;
   }
}
