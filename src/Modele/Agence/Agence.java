package Modele.Agence;


import Modele.GestionClient.Client;
import Modele.GestionDesBien.Bien;
import Modele.GestionDesBien.MandatVente;
import Modele.GestionPublicites.TypeAnnonce.Journaux;
import Modele.GestionPublicites.TypeAnnonce.Presse;
import Modele.GestionPublicites.TypeAnnonce.SiteInternet;
import Modele.GestionRdv.Agenda;
import Modele.GestionVente.Notaire;
import Modele.GestionVente.PromesseVente;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;

public class Agence {
   //Attribus
   private final String nomAgence;
   private float sommeAgence;
   //Agenda Agence
   private Agenda monAgenda = new Agenda();
   //Bien
   private HashMap<Bien, Client> listBienEnVente = new HashMap<>();                //Le bien et le nom du vendeur
   private HashMap<Client, Bien> listAcheteur = new HashMap<>();                   //L'acheteur et le bien qu'il désire
   private HashMap<Bien, PromesseVente> listBienVendue = new HashMap<>();   //Ajoute tous les bien Vendue

   //Vendeur
   private ArrayList<Client> vendeursList= new ArrayList<Client>();   //Liste de nos vendeurs

   //Publicités
   private ArrayList<Journaux> listJournaux= new ArrayList<Journaux>();
   private ArrayList<SiteInternet> listSiteInternet= new ArrayList<SiteInternet>();
   private ArrayList<Presse> listPresse= new ArrayList<Presse>();

   //Constructeur
   public Agence(String nomAgence){
      this.nomAgence = nomAgence;
   }

   //Asseceurs
   //Getter
   public float getSommeAgence() { return sommeAgence; }

   //Setter
   private void setListAcheteur(HashMap<Client, Bien> listAcheteur) {
      this.listAcheteur = listAcheteur;
   }

   public String getNomAgence() { return nomAgence; }

   public void augmenterSommeAgence(float sommeAgence) { this.sommeAgence += sommeAgence; }


   //Methodes

   public void prendreRdvClient( Client monClient, LocalDate dateClientSouhaite, LocalTime heureClientSouhaite){
      //Le client devra s'adapter au préalable du rdv qui sera pris par l'agence
      this.monAgenda.ajouterRdv(dateClientSouhaite,heureClientSouhaite);
      if (monClient.getStatus() == Client.Status.VENDEUR){
         monClient.accepteRdvVendeur();   //Accepte si c'est un vendeur
      }else{
         monClient.accepteRdvAcheteur();  //Accepte si c'est un acheteur
      }
   }

   public void effectuerAchatBien(Client monAcheteur,Client monVendeur,float sommeVerserAuVendeur){
      monAcheteur.diminuerSommeClient(sommeVerserAuVendeur);
      monVendeur.augmenterSommeClient(sommeVerserAuVendeur);
   }

   public void ClearAgence(){}

   //Vendeur

   public void getBienEnVente(){
      String res = "";
      String newLine = System.getProperty("line.separator");
      for (Bien bien : this.listBienEnVente.keySet()){
         res += bien.descriptionBien() + newLine;
      }
      System.out.println("Bien de l'agence" + newLine + res + newLine);
   }

   private void ajouterBienVendeur(Client monVendeur, Bien bienDuVendeur){
      this.listBienEnVente.put(bienDuVendeur,monVendeur);
      Journaux articleBien = new Journaux(bienDuVendeur.getRenseignementSupplementaire(),true,true);
      Presse articlePresse = new Presse(bienDuVendeur.getRenseignementSupplementaire(), true);
      SiteInternet pageInternet = new SiteInternet(bienDuVendeur.getRenseignementSupplementaire(),true,this.nomAgence);

      this.listJournaux.add(articleBien);
      this.listPresse.add(articlePresse);
      this.listSiteInternet.add(pageInternet);

   }

   public void mettreBienEnVente(Client monVendeur, Bien bienDuVendeur, LocalDate dateRdv, LocalTime heureRdv, LocalTime duree){
      if (monVendeur.getStatus() == Client.Status.VENDEUR) {   //On verifie si le client est bien un vendeur
         //System.out.println("Bonjour," + monVendeur.getNom() + "Entrer dans aGENCE");
         this.ajouterBienVendeur(monVendeur, bienDuVendeur);      //Ajoute le bien du vendeur à notre liste de bienVendeur
         int fraisDeVente = this.calculeFraisDeVente(bienDuVendeur);
         //On ajoute les frais de vente
         bienDuVendeur.setFraisDeVente(fraisDeVente);

         this.vendeursList.add(monVendeur);                       //Ajoute le vendeur à notre liste de vendeur
         this.prendreRdvClient(monVendeur,dateRdv,heureRdv);      //prend un rdv pour signer le mandat de vente
         //Creation d'un Mandat de Vente
         MandatVente accordMandatVente = new MandatVente(bienDuVendeur.getPrix(), dateRdv, heureRdv, duree);
         //Si le mandat de vente n'a pas été signé
         if (!monVendeur.getSignatureMandatVente()) {
            //Signature du mandat de vente par le client --> Vendeur
            accordMandatVente.signerMandatVente(monVendeur);
         }

      }
      monVendeur.sInscrireALaVente();
   }
   private int calculeFraisDeVente(Bien monBien){
      //On suppose que les frais de vente sont de 10% du prix
      return monBien.getPrix() * 10/100;
   }

   //Acheteur
   public void vendreBien(Notaire leNotaire, Client monAcheteur, Bien monBien, LocalDate dateDeVente, LocalTime heureDeVente){
      float commissionAgence = monBien.getPrix() * 7 /100;
      float prixVerserAuVendeur = monBien.getPrix() * 83/100;

      for (Bien bien : this.listBienEnVente.keySet()){
         if (bien  == monBien){ //Si monBien correspond
            Client monVendeur = this.listBienEnVente.get(monBien);

            //Creation d'une promesse de vente
            PromesseVente ficPromessedeVente = new PromesseVente(prixVerserAuVendeur,leNotaire.getAdresseNotaire(),dateDeVente,monBien.getFraisDeVente(),commissionAgence);
            ficPromessedeVente.SignerPromesseDeVente(monAcheteur,monVendeur); //Signature de la promesse de vente
            monAcheteur.verser10PoucentagePrix(monBien);                      //L'acheteur verse 10 %

            this.prendreRdvClient(monAcheteur,dateDeVente,heureDeVente);   //Rdv pour l'acheteur
            this.prendreRdvClient(monVendeur,dateDeVente,heureDeVente);    //Rdv pour le vendeur

            if (!monAcheteur.getOctroiPretImmo()){
               leNotaire.verserCommissionAgence(this,commissionAgence);
            }else{
               this.augmenterSommeAgence(commissionAgence);
               this.effectuerAchatBien(monAcheteur,monVendeur,prixVerserAuVendeur);
            }
         }
      }
   }

   public void contacterAcheteurPourBien(Client monAcheteur,Bien leBien){
      //Contacte un acheter et lui fait savoir qu'il y'a un bien qui le correspond
      monAcheteur.ajouteUnBienCorrespondant(leBien,this);
   }

   public void EnregistrerAppelleAcheteur(Client monAcheteur, Bien monBienDesire, LocalDate dateVisite, LocalTime heureVisite){
      //Enregistre l'appel d'un acheteur et  le type de bien qu'il désire

      this.listAcheteur.put(monAcheteur,monBienDesire);

      for (Bien bien : this.listBienEnVente.keySet()) { //Pour chaque bien qu'on a vérifier si on a pas de bien correspondant pour notre acheteur
         if (bien == monBienDesire){
            this.contacterAcheteurPourBien(monAcheteur,bien);
            this.prendreRdvClient(monAcheteur,dateVisite,heureVisite);
         }
      }
   }

   public void faireVisiter(Client monAcheteur){

      if(monAcheteur.getUnRendezVousAcheteur()){
         monAcheteur.accepteeUneVisite();
      }
   }

   public void supprimerAcheteur(Client monAcheteur,Bien bienDeAcheteur){
      if(monAcheteur.getAchatAcheteur()){
         for (Client i : this.listAcheteur.keySet()) {
            if (monAcheteur == i){
               this.listAcheteur.remove(i,bienDeAcheteur);
            }
         }
      }
   }

   public void getAcheteur(){
      String res = "";
      String newLine = System.getProperty("line.separator");
      for (Client acheteur : this.listAcheteur.keySet()){
         res += acheteur.getNom() + ": "+this.listAcheteur.get(acheteur).getTitre() +" prix :"+this.listAcheteur.get(acheteur).getPrix()+" euros." + newLine;
      }
      System.out.println("Mes Acheteur" + newLine + res + newLine);
   }


}
