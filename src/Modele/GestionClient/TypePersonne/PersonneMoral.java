package Modele.GestionClient.TypePersonne;

import Modele.GestionClient.Client;

public class PersonneMoral extends Client {
   //Attribus
   public enum Forme{SARL,EURL,INDIVIDUELLE,SAS, SASU, EIRL, AUTRE}
   private Forme formeJuridicte;
   private String numSiren;


   //Constructeur
   public PersonneMoral(Client.Status Status, String nom, String adresse, String tel, String email, Forme formeJuridicte, String numSiren){
      super(Status,nom,adresse,tel,email);
      this.formeJuridicte = formeJuridicte;
      this.numSiren = numSiren;
   }

   //Asseceurs
   //Getter
   public Forme getFormeJuridicte() { return formeJuridicte; }
   public String getNumSiren() { return numSiren; }

   //Setter

   public void setFormeJuridicte(Forme formeJuridicte) {
      this.formeJuridicte = formeJuridicte;
   }

   public void setNumSiren(String numSiren) {
      this.numSiren = numSiren;
   }
}
