package Modele.GestionClient;

import Modele.Agence.Agence;
import Modele.GestionDesBien.Bien;
import Modele.GestionVente.Notaire;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Client {


	//Attributs Client
	public enum Status {ACHETEUR,VENDEUR}
	private Status status;
	private String nom;
	private String adresse;
	private String tel;
	private String email;

	//Attributs Vendeur
	private boolean inscriptionALaVente = false;
	private ArrayList<Bien> corpsBienEnregistrer= new ArrayList<Bien>();
	private boolean signatureMandatVente = false;
	private boolean aUnRendezVousVendeur = false;
	private boolean signaturePromesseVenteVendeur = false;
	private float sommeVendeur;

	//Attribus Acheteur
	private HashMap<Bien, Agence> listBienCorrespondant;
	private boolean octroiPretImmo = false;
	private boolean aUnRendezVousAcheteur = false;
	private boolean aUneVisite = false;
	private  boolean decisionAcheteur = false;
	private boolean aAcheteUnBien = false;
	private boolean signaturePromesseVenteAcheteur = false;
	private float sommeAcheteur;



	//Contructeurs
	public Client (Status Status,String nom,String adresse,String tel,String email) {
		this.nom = nom;
		this.adresse = adresse;
		this.tel = tel;
		this.email = email;
		this.setMonStatus(Status);
	}

	//Assesseur

	//Getter Client
	public String getNom() {return this.nom;}
	public String getAdresse(){return this.adresse;}
	public String getTel(){return this.tel;}
	public String getEmail() {return this.email;}
	public Status getStatus() { return this.status;}


	//Getter Acheteur
	public boolean getOctroiPretImmo() { return this.status == Status.ACHETEUR && octroiPretImmo;}
	private boolean getUneVisite() { return this.status == Status.ACHETEUR && aUneVisite;}
	public boolean getUnRendezVousAcheteur(){ return this.status == Status.ACHETEUR &&  this.aUnRendezVousAcheteur;}
	public boolean getAchatAcheteur(){return this.status == Status.ACHETEUR && aAcheteUnBien;}
	private boolean getSignaturePromesseVenteAcheteur() { return status == Status.ACHETEUR && signaturePromesseVenteAcheteur; }

	//Getter Vendeur
	public List<Bien> getCorpsBienEnregistrer() {
		if (this.status == Status.VENDEUR) { return this.corpsBienEnregistrer;}
		return null;
	}
	public boolean getSignatureMandatVente(){ return status == Status.VENDEUR && this.signatureMandatVente;}
	public boolean isaUnRendezVousVendeur(){ return this.aUnRendezVousVendeur;}
	public boolean getSignaturePromesseVenteVendeur() { return status == Status.VENDEUR && signaturePromesseVenteVendeur; }

	//Setter Client
	public void setMonStatus(Status monStatus) { this.status = monStatus;}

	//Setter Acheteur
	public void accepteeUneVisite(){ if (this.status == Status.ACHETEUR){this.aUneVisite = true;}}
	public void accepteAchatBien(){if (this.status == Status.ACHETEUR){this.aAcheteUnBien = true;}}

	//Methodes
	//Client
	public void direBonjour() {
		String newLine = System.getProperty("line.separator");
		System.out.println("Bonjour, je m'apppele, "+this.getNom()+ ", je suis un " + this.status + newLine);
	}

	public void diminuerSommeClient(float valeur){
		if (this.status == Status.ACHETEUR){
			this.sommeAcheteur -= valeur;
		}
	}

	public void augmenterSommeClient(float valeur){
		if (this.status == Status.ACHETEUR){
			this.sommeAcheteur += valeur;
		}
	}


	//Methode Vendeur

	public void accepteRdvVendeur(){ if (this.status == Status.VENDEUR){this.aUnRendezVousVendeur = true;} }

	public void sInscrireALaVente(){
		if (this.status == Status.VENDEUR){
			this.inscriptionALaVente = true;
		}
	}

	public void signerMandatVente(){ this.signatureMandatVente = true; }

	public void enregistrerBien(Bien monBien, Agence monAgence, LocalDate dateRdv, LocalTime heureRdv, LocalTime duree) {
		if (status == Status.VENDEUR){
			this.corpsBienEnregistrer.add(monBien);
			monAgence.mettreBienEnVente(this,monBien,dateRdv,heureRdv,duree);
			//this.signerMandatVente();
			System.out.println("Votre " +monBien.getTypeMonBien() + " a bien ete enregistre!");
		}
	}

	public void getBienEnregistrer(){
		String newLine = System.getProperty("line.separator");
		String res = "";
		if (status == Status.VENDEUR){
			for(Bien unBien : corpsBienEnregistrer){
				res += unBien.getTypeMonBien()+ ": " +unBien.getTitre() + newLine;
			}
			System.out.println(res + newLine);
		}
	}

	public void signerPromesseVenteVendeur(){
		if (this.status == Status.VENDEUR){
			this.signaturePromesseVenteVendeur = true;
		}
	}


	//Methodes Acheteur
	public void accepteRdvAcheteur(){ if (this.status == Status.ACHETEUR){this.aUnRendezVousAcheteur= true; }}

	public void acheterBien(Bien monBien, Agence monAgence, Notaire monNotaire, LocalDate dateDeVente, LocalTime heureDevente) {
		if (status == Status.ACHETEUR) {
			this.decisionAcheteur = true;
			monAgence.vendreBien(monNotaire, this, monBien, dateDeVente, heureDevente);
		}
	}

	public String ajouteUnBienCorrespondant(Bien leBien,Agence monAgence){
		this.listBienCorrespondant.put(leBien,monAgence);
		return "J'ai eu un bien qui me correspond de " + monAgence.getNomAgence();
	}

	public void signerPromesseVenteAcheteur(){
		if (this.status == Status.ACHETEUR){
			this.signaturePromesseVenteAcheteur = true;
		}
	}

	public Integer verser10PoucentagePrix(Bien bienAgence){
		return this.octroiPretImmo ? bienAgence.getPrix()*10/100:0;
	}

	public String contacterAgence(Agence monAgence, Bien monBienDesire, LocalDate dateVisite, LocalTime heureVisite){
		if (status == Status.ACHETEUR){
		monAgence.EnregistrerAppelleAcheteur(this,monBienDesire,dateVisite,heureVisite);
		return this.getNom()+"appelle" +monAgence.getNomAgence();
		}
		return "Tu n'est pas un Acheteur";
	}

}

