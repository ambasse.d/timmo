package Modele.GestionDesBien;

import java.time.LocalDate;

public class Bien {
   //Attribus
   public enum TypeBien {APPARTEMENT,TERRAIN,MAISON}
   private TypeBien typeMonBien;
   private String titre;
   private int prix;
   private LocalDate dateVente;
   private int Xorientation;
   private int Yorientation;
   private String renseignementSupplementaire;
   private int numeroBien;
   private int fraisDeVente;



   //Constructeurs
   public Bien(String titre, int prix, LocalDate dateVente, int xorientation, int yorientation , String renseignementSupplementaire){
      //this.typeMonBien = typeMonBien;
      this.prix = prix;
      this.dateVente = dateVente;
      this.Xorientation = xorientation;
      this.Yorientation = yorientation;
      this.renseignementSupplementaire=renseignementSupplementaire;
      this.numeroBien = numeroBien;
      this.titre = titre;
   }

   //Accesseurs
   //Getter
   public TypeBien getTypeMonBien() {
      return typeMonBien;
   }
   public Integer getPrix() { return prix; }
   public LocalDate getDateVente() {
      return dateVente;
   }
   public Integer getXorientation() {
      return Xorientation;
   }
   public Integer getYorientation() {
      return Yorientation;
   }
   public String getRenseignementSupplementaire() {
      return renseignementSupplementaire;
   }
   public Integer getNumeroBien() {
      return numeroBien;
   }
   public int getFraisDeVente() { return fraisDeVente; }
   public String getTitre() { return titre; }

   //Setter
   public void setTypeMonBien(TypeBien typeMonBien) {
      this.typeMonBien = typeMonBien;
   }
   public void setFraisDeVente(int fraisVente){this.fraisDeVente = fraisVente;}

   public String descriptionBien(){
      String newLine = System.getProperty("line.separator");
      return this.getTitre()+": "+ this.getPrix()+", mis en vente le "+this.dateVente +newLine;

   }
}
