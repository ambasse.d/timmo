package Modele.GestionDesBien;

import Modele.GestionClient.Client;

import java.time.LocalDate;
import java.time.LocalTime;

public class MandatVente {
   //Attribus
   private int prixBien;
   private LocalDate dateVenteSouhaite;
   private LocalTime heureVenteSouhaite;
   private LocalTime duree;
   private boolean signatureMandatVente = false;

   //Constructeur
   public MandatVente(int prixBien, LocalDate dateVenteSouhaite, LocalTime  heureVenteSouhaite, LocalTime duree){
      this.dateVenteSouhaite = dateVenteSouhaite;
      this.heureVenteSouhaite = heureVenteSouhaite;
      this.prixBien = prixBien;
      this.duree = duree;
   }

   //Assesseur


   private int getPrixBien() { return prixBien; }

   private LocalDate getDateVenteSouhaite() { return dateVenteSouhaite; }

   private LocalTime getHeureVenteSouhaite() { return heureVenteSouhaite; }

   private LocalTime getDuree() { return duree; }

   //Methode
   public void signerMandatVente(Client monVendeur) {
      monVendeur.signerMandatVente();
      this.signatureMandatVente = true;
   }

   @Override
   public String toString() {
      return "MandatVente{" +
              "prixBien=" + prixBien +
              ", dateVenteSouhaite=" + dateVenteSouhaite +
              ", heureVenteSouhaite=" + heureVenteSouhaite +
              ", duree=" + duree +
              ", signatureMandatVente=" + signatureMandatVente +
              '}';
   }
}
