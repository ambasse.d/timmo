package Modele.GestionDesBien.typeBien;

import Modele.GestionDesBien.Bien;

import java.time.LocalDate;

public class Maison extends Bien {

   //Attribus
   public enum TypeChauffage{GAZ,ELECTRIQUE,BOIS}

   public int surfaceHabitable;
   public int nombrePiece;
   public int nombreEtage;
   public TypeChauffage moyenChauffage;
   public int surfaceSol;
   public int longueurFacade;



   //Constructeurs
   public Maison(String titre, int prix, LocalDate dateVente, int xorientation, int yorientation, String renseignementSupplementaire, int surfaceHabitable, int nombrePiece, int nombreEtage, TypeChauffage moyenChauffage, int surfaceSol, int longueurFacade) {

      super(titre,prix, dateVente, xorientation, yorientation, renseignementSupplementaire);
      super.setTypeMonBien(TypeBien.MAISON);
      this.surfaceHabitable = surfaceHabitable;
      this.nombrePiece = nombrePiece;
      this.nombreEtage = nombreEtage;
      this.moyenChauffage = moyenChauffage;
      this.surfaceSol = surfaceSol;
      this.longueurFacade = longueurFacade;
   }

   //Assesseur
   //Getter
   public int getSurfaceHabitable() {
      return surfaceHabitable;
   }

   public int getNombrePiece() {
      return nombrePiece;
   }

   public TypeChauffage getMoyenChauffage() {
      return moyenChauffage;
   }

   public int getSurfaceSol() {
      return surfaceSol;
   }

   public int getLongueurFacade() {
      return longueurFacade;
   }

   //Methodes
   public void decrireMaison() {
      System.out.println("C'est une "+this.getTypeMonBien()+" de "+this.getNombrePiece()+" pieces, qui vaut "+this.getPrix() + " euros et a une surface au sol de " + this.getSurfaceSol()+" m2.");
   }

}
