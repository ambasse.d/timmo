package Modele.GestionDesBien.typeBien;


import Modele.GestionDesBien.Bien;

import java.time.LocalDate;

public class Terrain extends Bien {
   //Attribus
   public int surfaceSol;
   public int longeurFacade;

   //Constructeurs
   public Terrain(String titre, int prix, LocalDate dateVente, int xorientation, int yorientation, String renseignementSupplementaire, int surfaceSol, int longeurFacade) {
      super(titre,prix, dateVente, xorientation, yorientation, renseignementSupplementaire);
      super.setTypeMonBien(TypeBien.TERRAIN);
      this.surfaceSol = surfaceSol;
      this.longeurFacade = longeurFacade;
   }

   //Assesseur

   //Getter

   public int getSurfaceSol() {
      return surfaceSol;
   }

   public int getLongeurFacade() {
      return longeurFacade;
   }

   //Methodes
   public void decrireTerrain() {
      System.out.println("C'est un "+this.getTypeMonBien()+" de "+this.getSurfaceSol()+" m2, qui vaut "+this.getPrix() + " euros et la longeur de la facede mesure " + this.getLongeurFacade()+" m2");
   }

}
