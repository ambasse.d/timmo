package Modele.GestionDesBien.typeBien;


import Modele.GestionDesBien.Bien;

import java.time.LocalDate;

public class Appartement extends Bien {
   //Attributs
   public int nombrePiece;
   public int nombreEtage;
   public int chargeMensuelle;



   //Constructeur
   public Appartement(String titre, int prix, LocalDate dateVente, int xorientation, int yorientation, String renseignementSupplementaire, int nombrePiece, int nombreEtage, int chargeMensuelle) {
      super( titre, prix, dateVente, xorientation, yorientation, renseignementSupplementaire);
      super.setTypeMonBien(TypeBien.APPARTEMENT);
      this.nombrePiece = nombrePiece;
      this.nombreEtage = nombreEtage;
      this.chargeMensuelle = chargeMensuelle;

   }

   //Assesseur
   //Getter


   public int getNombrePiece() {
      return nombrePiece;
   }

   public int getNombreEtage() {
      return nombreEtage;
   }

   public int getChargeMensuelle() {
      return chargeMensuelle;
   }

   //Methodes

   public void decrireAppartement() {
      System.out.println("C'est un "+this.getTypeMonBien()+" de "+this.getNombrePiece()+" pieces, qui vaut "+this.getPrix() + " euros et une charges mensuelles de " + this.getChargeMensuelle()+" euros");
   }
}
