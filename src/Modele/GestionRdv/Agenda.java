package Modele.GestionRdv;


import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;

public class Agenda {
   //Attribus
   private LocalDate date;
   private LocalTime time;
   private HashMap<LocalDate, LocalTime> corpsRdv= new HashMap<LocalDate, LocalTime>();
   //Contructeurs
   public Agenda(){

   }

   //Assesseurs

   public LocalDate getDate() { return date; }
   public LocalTime getTime() { return time; }
   public HashMap<LocalDate, LocalTime> getCorpsRdv() { return corpsRdv; }

   //Methodes
   public void ajouterRdv(LocalDate date,LocalTime heure){

      this.corpsRdv.put(date,heure);

   }
   public void supprimerRdv(LocalDate date,Time heure){
      this.corpsRdv.remove(date,heure);
   }
}
