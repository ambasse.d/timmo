package Modele.GestionPublicites;

public class AnnoncePublicitaire {
   //Attribus
   public enum TypePublibite {JOURNAUX,WEB,PRESSE}
   private String texteAnnonce;
   private boolean image;
   private TypePublibite typePublicite;

   //Contructeur
   public AnnoncePublicitaire(String texteAnnonce, Boolean image){
      this.texteAnnonce = texteAnnonce;
      this.image = image;
   }

   //Assesseur
   //Getter

   public String getTexteAnnonce() { return texteAnnonce;}
   public Boolean getImage() { return image;}

   //Setter

   public void setTypePublicite(TypePublibite typePublicite) {
      this.typePublicite = typePublicite;
   }

   //Methodes


}
