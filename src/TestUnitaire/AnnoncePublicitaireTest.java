package TestUnitaire;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import Modele.GestionPublicites.AnnoncePublicitaire;
import org.junit.Test;



public class AnnoncePublicitaireTest{

	static AnnoncePublicitaire pub1 = new AnnoncePublicitaire("welcome", false);
	static AnnoncePublicitaire pub2 = new AnnoncePublicitaire("Hi", true);
	
	@Test
	public void TexteTest() {
		AnnoncePublicitaire pub1test = new AnnoncePublicitaire("welcome", false);
		AnnoncePublicitaire pub2test = new AnnoncePublicitaire("welcome", false);
		assertEquals(pub1test.getTexteAnnonce(), pub1.getTexteAnnonce());
		assertNotEquals(pub1test.getTexteAnnonce(), pub2.getTexteAnnonce());
	}
	
	@Test
	public void TexteImage() {
		AnnoncePublicitaire pub1test = new AnnoncePublicitaire("welcome", false);
		AnnoncePublicitaire pub2test = new AnnoncePublicitaire("welcome", false);
		assertEquals(pub1test.getImage(), pub1.getImage());
		assertNotEquals(pub1test.getImage(), pub2.getImage());
	}
}
