package TestUnitaire;



import static org.junit.jupiter.api.Assertions.assertEquals;
import java.time.LocalDate;
import java.time.LocalTime;
import static org.junit.Assert.assertNotEquals;

import Modele.GestionRdv.Agenda;
import org.junit.Test;

public class AgendaTest {

	LocalDate date = LocalDate.now();
	LocalTime time = LocalTime.now();
	LocalDate date2 = LocalDate.now().minusDays(1).minusMonths(1);
	LocalTime time2 = LocalTime.now().minusHours(8);
	Agenda Ag1 = new Agenda();
	Agenda Ag2 = new Agenda();
	Agenda Ag3 = new Agenda();
	
	@Test
	public void AgendaTest() {
		
		assertNotEquals(Ag1.getDate(), Ag2.getDate());
		assertNotEquals(Ag3.getTime(), Ag2.getTime());
		assertEquals(Ag1.getDate(), Ag3.getDate());
		assertEquals(Ag1.getTime(), Ag3.getTime());
	}
}
