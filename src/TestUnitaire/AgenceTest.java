package TestUnitaire;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import Modele.Agence.Agence;
import org.junit.Test;



public class AgenceTest {

	Agence ag1 = new Agence("nomAgence");
	
	
	@Test
	public void TestNom() {
		Agence ag1Test = new Agence("nomAgence");
		assertEquals(ag1.getNomAgence(),ag1Test.getNomAgence());
	}
	
	@Test
	public void TestSomme() {
		assertEquals(ag1.getSommeAgence(),0);
		ag1.augmenterSommeAgence(15);
		assertNotEquals(ag1.getSommeAgence(),0);
		assertEquals(ag1.getSommeAgence(),15);
	}
	
	
}
