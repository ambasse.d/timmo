package TestUnitaire;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import Modele.Agence.Agence;
import Modele.GestionVente.Notaire;
import org.junit.Test;



public class NotaireTest {

	
	Notaire notaire1 = new Notaire("123 rue de la joie Paris");
	Agence ag = new Agence("Agencetest");
	@Test
	public void TestAdresse() {
		Notaire notaire1Test = new Notaire("123 rue de la joie Paris");
		Notaire notaire2Test = new Notaire("1234 rue de la joie Paris");
		assertEquals(notaire1.getAdresseNotaire(), notaire1Test.getAdresseNotaire());
		assertNotEquals(notaire1.getAdresseNotaire(), notaire2Test.getAdresseNotaire());
	}
	
	@Test
	public void commissionVerseeTest() {
		assertFalse(notaire1.comissionEstVersee());
	}
	/*
	@Test
	public void verserCommissionAgenceTest() {
		Agence agTest = new Agence("Agencetest");
		assertEquals(ag, agTest);
		assertEquals(notaire1.verserCommissionAgence(ag, 0),notaire1.verserCommissionAgence(agTest, 0));
		assertNotEquals(notaire1.verserCommissionAgence(15), 16);
		assertTrue(notaire1.comissionEstVersee());
	}
	*/
}
