package TestUnitaire;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.time.LocalDate;

import Modele.GestionDesBien.Bien;
import org.junit.jupiter.api.Test;


public class TestBien {

	LocalDate date = LocalDate.now();
	LocalDate date2 = LocalDate.now().minusMonths(8);
	Bien bien1 = new Bien("maison", 10, date, 15, 14, "maison 2 �tages");
	Bien bien2 = new Bien("appartement", 5, date2, 80, -9, "appartement 1 �tage");
	Bien testbien1 = new Bien("maison", 10, date, 15, 14, "maison 2 �tages");
	//Bien bien = new Bien(titre, prix, dateVente, xorientation, yorientation, renseignementSupplementaire)
			
	@Test
	public void TestTitre() {
		assertEquals(bien1.getTitre(), testbien1.getTitre());
		assertNotEquals(bien1.getTitre(), bien2.getTitre());
		
	}
	
	@Test
	public void TestPrix() {
		assertEquals(bien1.getPrix(), testbien1.getPrix());
		assertNotEquals(bien1.getPrix(), bien2.getPrix());
	}
	
	@Test
	public void TestdateVente() {
		assertEquals(bien1.getDateVente(), testbien1.getDateVente());
		assertNotEquals(bien1.getDateVente(), bien2.getDateVente());
	}
	
	@Test
	public void TestOrientation() {
		assertEquals(bien1.getXorientation(), testbien1.getXorientation());
		assertNotEquals(bien1.getXorientation(), bien2.getXorientation());
		assertEquals(bien1.getYorientation(), testbien1.getYorientation());
		assertNotEquals(bien1.getYorientation(), bien2.getYorientation());
	}
	
	@Test
	public void TestString() {
		assertEquals(bien1.getRenseignementSupplementaire(), testbien1.getRenseignementSupplementaire());
		assertNotEquals(bien1.getRenseignementSupplementaire(), bien2.getRenseignementSupplementaire() );
	}
}
