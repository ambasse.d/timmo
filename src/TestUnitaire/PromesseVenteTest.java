package TestUnitaire;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import Modele.GestionVente.PromesseVente;
import org.junit.Test;



public class PromesseVenteTest {

	 LocalDate date = LocalDate.now();
	PromesseVente promesse1 = new PromesseVente(15, "123 rue", date, 10, 5);
	
	@Test
	public void prixTest() {
		PromesseVente promesse1Test = new PromesseVente(15, "123 rue", date, 10, 5);
		PromesseVente promesse2Test = new PromesseVente(14, "123 rue", date, 10, 5);
		assertEquals(promesse1.getPrixVerserAuVendeur(),promesse1Test.getPrixVerserAuVendeur());
		assertNotEquals(promesse1.getPrixVerserAuVendeur(),promesse2Test.getPrixVerserAuVendeur());
	}
	
	@Test
	public void AdresseTest() {
		PromesseVente promesse1Test = new PromesseVente(15, "123 rue", date, 10, 5);
		PromesseVente promesse2Test = new PromesseVente(15, "12 rue", date, 10, 5);
		assertEquals(promesse1.getAdresseNotaire(),promesse1Test.getAdresseNotaire());
		assertNotEquals(promesse1.getAdresseNotaire(),promesse2Test.getAdresseNotaire());
	}
	
	@Test
	public void DateTest() {
		LocalDate dateTest = date.minusDays(1);
		PromesseVente promesse1Test = new PromesseVente(15, "123 rue", date, 10, 5);
		PromesseVente promesse2Test = new PromesseVente(15, "123 rue", dateTest, 10, 5);
		assertEquals(promesse1.getDateVente(),promesse1Test.getDateVente());
		assertNotEquals(promesse1.getDateVente(),promesse2Test.getDateVente());
	}
	
	@Test
	public void FraisTest() {
		PromesseVente promesse1Test = new PromesseVente(15, "123 rue", date, 10, 5);
		PromesseVente promesse2Test = new PromesseVente(15, "123 rue", date, 11, 5);
		assertEquals(promesse1.getFraisVente(),promesse1Test.getFraisVente());
		assertNotEquals(promesse1.getFraisVente(),promesse2Test.getFraisVente());
	}
	
	@Test
	public void ComissionTest() {
		PromesseVente promesse1Test = new PromesseVente(15, "123 rue", date, 10, 5);
		PromesseVente promesse2Test = new PromesseVente(15, "123 rue", date, 11, 6);
		assertEquals(promesse1.getCommissionAgence(),promesse1Test.getCommissionAgence());
		assertNotEquals(promesse1.getCommissionAgence(),promesse2Test.getCommissionAgence());
	}
		
}
