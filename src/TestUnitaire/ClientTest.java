package TestUnitaire;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


import Modele.GestionClient.Client;
import org.junit.jupiter.api.Test;


public class ClientTest {
		
	Client c1 = new Client(Client.Status.ACHETEUR, "ben", "31", "0656", "email@email");
	Client c2 = new Client(Client.Status.VENDEUR, "Amb", "78", "0678", "email2@email");


	
	@Test
	public void testStatus(){
		Client testc1 = new Client(Client.Status.ACHETEUR, "", "", "", "");
		Client testc2 = new Client(Client.Status.VENDEUR, "", "", "", "");;
		assertEquals(c1.getStatus(),testc1.getStatus());
		assertEquals(c2.getStatus(),testc2.getStatus());
		assertNotEquals(c1.getStatus(),testc2.getStatus());
		assertNotEquals(c2.getStatus(),testc1.getStatus());
	}
		
	@Test
	public void testNom(){
		String nom1 = "ben";
		String nom2 = "Amb";
		assertEquals(c1.getNom(),nom1);
		assertEquals(c2.getNom(),nom2);
		assertNotEquals(c1.getNom(),nom2);
		assertNotEquals(c2.getNom(),nom1);
	}

	
}
