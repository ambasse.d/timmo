package Controleur;



import Modele.Agence.Agence;
import Modele.GestionClient.Client;
import Modele.GestionClient.DesireAcheteur;
import Modele.GestionClient.TypePersonne.PersonneMoral;
import Modele.GestionClient.TypePersonne.PersonnePhysique;
import Modele.GestionDesBien.typeBien.Appartement;
import Modele.GestionDesBien.typeBien.Maison;
import Modele.GestionDesBien.typeBien.Terrain;

import java.time.LocalDate;
import java.time.LocalTime;

////////////////Programme Principal///////////////
public class MainGestionTimmo {
	public static void main(String[] args) {
		//Initialisation des éléments
		Agence AgenceToctoc = new Agence("Toctoc");

		Agence AgenceTimmo = new Agence("Timmo");

		Client Vendeur1 = new PersonnePhysique(
				Client.Status.VENDEUR,
				"Yourik",
				"16 Rue de la comedie Toulouse",
				"06",
				"shcjs@cj.s");
		Client VendeurPro1 =new PersonneMoral(Client.Status.VENDEUR,
				"Yaki",
				"77 rue de x",
				"06369523",
				"scb@fd.ds",
				PersonneMoral.Forme.SARL,
				"656332265");

		Client Acheteur1 = new PersonnePhysique(
				Client.Status.ACHETEUR,
				"Bernard",
				"1 place de la republique Auch",
				"054225555",
				"jybu@gh.vg");

		Client AcheteurPro1 = new PersonneMoral(Client.Status.ACHETEUR,
				"tic",
				"45 avenue de d",
				"32385336",
				"dhfdj@difjd.d",
				PersonneMoral.Forme.SAS,
				"455865472");

		Maison MaisonVendeurPro1 = new Maison("Maison 4 chambre",
				13500,
				LocalDate.now(),
				125225,
				12586,
				"Belle sur mer",
				153,
				5,
				1,
				Maison.TypeChauffage.GAZ,
				452,
				258);

		Appartement AppartementVendeur1= new Appartement("Appartement T3",
				12000,
				LocalDate.now(),
				152855,
				2852244,
				"Appartement avec balcon situé au centre ville",
				1523,
				3,
				0);

		DesireAcheteur AppartementDesireAcheteur1= new DesireAcheteur("toto",25000,LocalDate.now(),12555,12585,"Belle vue");

		Terrain TerrainVendeur1 = new Terrain("Terrain Exterieur",
				4000,
				LocalDate.now(),
				1552586,
				69996699,
				"Elle se situe en périphérique de toulouse près de l'espace Gramont",
				125,
				3562);



		//Utilisation des méthodes

		//Vendeur1 propose un bien
		Vendeur1.direBonjour();
		//Enregistre l'appart1
		Vendeur1.enregistrerBien(AppartementVendeur1,AgenceTimmo, LocalDate.now(), LocalTime.now(),LocalTime.of(1,0));

		//Enregistre le terrain1
		Vendeur1.enregistrerBien(TerrainVendeur1,AgenceTimmo,LocalDate.now(),LocalTime.now(),LocalTime.of(1,0));

		//Detail des Bien du vendeur
		Vendeur1.getBienEnregistrer();

		//Detail des ventes que l'agence possede
		AgenceTimmo.getBienEnVente();

		//L'acheteur pro contacte l'agence
		AcheteurPro1.contacterAgence(AgenceTimmo,AppartementDesireAcheteur1,LocalDate.now(),LocalTime.now());

		//Liste des acheteurs ayant contacté l'agence
		AgenceTimmo.getAcheteur();







	}

}
